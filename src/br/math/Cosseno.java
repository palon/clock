/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.math;

import java.math.BigDecimal;

/**
 * Implementa função de Seno (Sen α = CO / HIP)
 * 
 * @author Paulo
 */
public class Cosseno {
    private double alpha;
    private final double ca;
    private final double hyp;
    private double cos;
    
    /**
     * Calcula o cosseno e o ângulo de um triângulo retângulo com base em seu
     * Cateto Adjascente e sua Hipotenusa.
     * Se ca ou hyp for negativo, é automaticamente invertido.
     * 
     * @param ca Cateto Adjascente
     * @param hyp Hipotenusa
     */
    public Cosseno(double ca, double hyp) {
        if (ca > hyp)
            throw new IllegalArgumentException("Um cateto nunca é maior que a hipotenusa");
        
        this.ca = ca < 0 ? new BigDecimal(ca).negate().doubleValue() : ca;
        this.hyp = hyp < 0 ? new BigDecimal(ca).negate().doubleValue() : hyp;
        calcCos();
        calcAlpha();
    }

    public double getAlpha() {
        return alpha;
    }

    public double getCa() {
        return ca;
    }

    public double getHyp() {
        return hyp;
    }

    public double getCos() {
        return cos;
    }
    
    private void calcAlpha() {
        alpha = Math.toDegrees(Math.acos(cos)); 
    }
    
    private void calcCos() {
        cos = ca / hyp;
    }
    
    public static double getCatetoAdjascente(double ang, double hip) {
        ang = Math.toRadians(ang);        
        double cos = Math.cos(ang);        
        return cos * hip;
    }

    @Override
    public String toString() {
        return "Cosseno = " + Double.toString(cos) + " / α = " + Double.toString(alpha);
    }
    
    public static void main(String[] args) {
        System.out.println(Cosseno.getCatetoAdjascente(50, 100));
    }
}
