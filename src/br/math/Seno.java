/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.math;

import java.math.BigDecimal;

/**
 * Implementa função de Seno (Sen α = CO / HIP)
 * 
 * @author Paulo
 */
public class Seno {
    private double alpha;
    private final double co;
    private final double hyp;
    private double sin;
    
    /**
     * Calcula o seno e o ângulo de um triângulo retângulo com base em seu
     * Cateto Oposto e sua Hipotenusa.
     * Se co ou hyp for negativo, é automaticamente invertido.
     * 
     * @param co Cateto Oposto
     * @param hyp Hipotenusa
     */
    public Seno(double co, double hyp) {
        if (co > hyp)
            throw new IllegalArgumentException("Um cateto nunca é maior que a hipotenusa");
        
        this.co = co < 0 ? new BigDecimal(co).negate().doubleValue() : co;
        this.hyp = hyp < 0 ? new BigDecimal(co).negate().doubleValue() : hyp;
        calcSin();
        calcAlpha();
    }

    public double getAlpha() {
        return alpha;
    }

    public double getCo() {
        return co;
    }

    public double getHyp() {
        return hyp;
    }

    public double getSin() {
        return sin;
    }
    
    private void calcAlpha() {
        alpha = Math.toDegrees(Math.asin(sin)); 
    }
    
    private void calcSin() {
        sin = co / hyp;
    }
    
    /**
     * Retorna o valor do Cateto Oposto com base no ângulo e hipotenusa
     * @param ang Medida do Ângulo
     * @param hip Medida da Hipotenusa
     * @return Medida do Cateto Oposto
     */
    public static double getCatetoOposto(double ang, double hip) {
        ang = Math.toRadians(ang);        
        double seno = Math.sin(ang);        
        return seno * hip;
    }

    @Override
    public String toString() {
        return "Sin = " + Double.toString(sin) + " / α = " + Double.toString(alpha);
    }
    
    public static void main(String[] args) {
        System.out.println(Seno.getCatetoOposto(50, 100));
    }
}
