/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.math;

import java.math.BigDecimal;

/**
 * Implementa o Teorema de Pitágoras
 * 
 * @author Paulo
 */
public class Pitagoras {
    private double a;
    private double b;
    private double c;
    private final int calc;
    
    public static final int A = 0;
    public static final int C = 1;

    /**
     * Calcula o valor da Hipotenusa ou Cateto.
     * Se x ou y for negativo, é automaticamente invertido.
     * 
     * @param x Hipotenusa A ou Cateto B
     * @param y Catego B ou Catego C
     * @param calc Variável a ser calculada, deve ser informado com Pythagoras.A ou Pythagoras.B
     */
    public Pitagoras(double x, double y, int calc) {
        this.calc = calc;
        
        if (calc == A) {
            this.b = x < 0 ? new BigDecimal(x).negate().doubleValue() : x;
            this.c = y < 0 ? new BigDecimal(y).negate().floatValue() : y; 
            calcA();
        } else {
            if (y > x)
                throw new IllegalArgumentException("Um cateto nunca é maior que a hipotenusa");
            
            this.a = x < 0 ? new BigDecimal(x).negate().doubleValue() : x;
            this.b = y < 0 ? new BigDecimal(y).negate().floatValue() : y; 
            calcC();
        }
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }
    
    private void calcA() {        
        a = Math.sqrt(Math.pow(b, 2) + Math.pow(c, 2));
    }
    
    private void calcC() {
        c = Math.sqrt(Math.pow(a, 2) - Math.pow(b, 2));
    }
    
    @Override
    public String toString() {
        if (calc == A)
            return Double.toString(a);
        else
            return Double.toString(c);
    }
    
    public static void main(String[] args) {
        System.out.println(new Pitagoras(10, 22.36, C));
    }
}
