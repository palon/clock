/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.engine;

import br.math.Cosseno;
import br.math.Pitagoras;
import br.math.Seno;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.math.BigDecimal;

/**
 *
 * @author Paulo
 */
public class Minuto {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    
    private double x;
    private double y;    
    private int minuto;
    
    public Minuto(int x, int y) {
        this.x = x;
        this.y = y;
        
        setMinuto();
    }

    public void setX(double x) {
        double old = this.x;
        this.x = x;
        changeSupport.firePropertyChange("x", old, x);
        
        setMinuto();
    }

    public double getX() {
        return x;
    }

    public void setY(double y) {
        double old = this.y;
        this.y = y;
        changeSupport.firePropertyChange("y", old, y);
        
        setMinuto();
    }
    
    public void setMinuto(int minuto) {
        double[] xy = getXYByMinuto(minuto);
        
        setX(xy[0]);
        setY(xy[1]);
    }
    
    private void setMinuto() {
        Quadrante q = Quadrante.getByXY(x, y);
        
        double t = new Pitagoras(x, y, Pitagoras.A).getA();        
        double a;
        
        if (q.getNum() % 2 == 0)
            a = new Cosseno(x, t).getAlpha();
        else
            a = new Seno(x, t).getAlpha();
        
        a += q.getAng();
        
        a -= a % 6;
        a /= 6;
        
        int old = this.minuto;
        minuto = (int) a;
        changeSupport.firePropertyChange("minuto", old, minuto);
    }
    
    public int getMinuto() {
        return minuto;
    }
    
    public static double[] getXYByMinuto(int minuto) {
        if (minuto < 0 || minuto > 59)
            throw new IllegalArgumentException("O minuto deve estar entre 0 e 59");
        
        Quadrante q = Quadrante.getByMinuto(minuto);
        
        double x = 0;
        double y = 0;
        
        switch (q.getNum()) {
            case 1:
                x = Seno.getCatetoOposto(q.getMinutos().get(minuto), 75);
                y = Cosseno.getCatetoAdjascente(q.getMinutos().get(minuto), 75);
                break;
                
            case 2:
                x = Cosseno.getCatetoAdjascente(q.getMinutos().get(minuto), 75);
                y = new BigDecimal(Seno.getCatetoOposto(q.getMinutos().get(minuto), 75)).negate().doubleValue();
                break;
                
            case 3:
                x = new BigDecimal(Seno.getCatetoOposto(q.getMinutos().get(minuto), 75)).negate().doubleValue();
                y = new BigDecimal(Cosseno.getCatetoAdjascente(q.getMinutos().get(minuto), 75)).negate().doubleValue();
                break;
                
            case 4:
                x = new BigDecimal(Cosseno.getCatetoAdjascente(q.getMinutos().get(minuto), 75)).negate().doubleValue();
                y = Seno.getCatetoOposto(q.getMinutos().get(minuto), 75);
                break;
        }
        
        return new double[]{x, y};
    }

    @Override
    public String toString() {
        return Integer.toString(getMinuto());
    }
    
    public void addPropertyChangeListener(PropertyChangeListener l) {
        changeSupport.addPropertyChangeListener(l);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changeSupport.removePropertyChangeListener(l);
    }
}
