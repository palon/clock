/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.engine;

import br.math.Cosseno;
import br.math.Pitagoras;
import br.math.Seno;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Paulo
 */
public class Hora {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    
    private double x;
    private double y;
    
    /**
     * Indica se o relógio está trabalhando em modo AM/PM ou 24 horas, onde true = AM/PM
     */
    private boolean ampm;
    
    /**
     * Caso o relógio esteja trabalhando em modo AM/PM, indica se o horário
     * deve ser selecionado em AM ou PM, onde true = AM
     */
    private boolean pm;
    
    private int hora;

    public Hora(double x, double y) {
        this.x = x;
        this.y = y;
        
        setHora();
    }

    public void setX(double x) {
        double old = this.x;
        this.x = x;
        changeSupport.firePropertyChange("x", old, x);
        
        setHora();
    }

    public double getX() {
        return x;
    }

    public void setY(double y) {
        double old = this.y;
        this.y = y;
        changeSupport.firePropertyChange("y", old, y);
        
        setHora();
    }

    public double getY() {
        return y;
    }
    
    public boolean isAmpm() {
        return ampm;
    }

    public void setAmpm(boolean ampm) {
        boolean old = this.ampm;
        this.ampm = ampm;
        changeSupport.firePropertyChange("ampm", old, ampm);
        
        if (!ampm && hora > 12)
            setPm(pm);
        
        setHora();
    }

    public boolean isPm() {
        return pm;
    }

    public void setPm(boolean pm) {
        boolean old = this.pm;
        this.pm = pm;
        changeSupport.firePropertyChange("pm", old, pm);
        
        setHora();
    }
    
    public void setHora(int hora) {
        double[] xy = getXYByHora(hora);
        
        setX(xy[0]);
        setY(xy[1]);
    }
    
    private void setHora() {
        Quadrante q = Quadrante.getByXY(x, y);
        
        double t = new Pitagoras(x, y, Pitagoras.A).getA();        
        double a;
        
        if (q.getNum() % 2 == 0)
            a = new Cosseno(x, t).getAlpha();
        else
            a = new Seno(x, t).getAlpha();
        
        a = new BigDecimal(a).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
        
        a += q.getAng();
        
        a -= a % 30;
        a /= 30;
        
        if ((!ampm && pm) || (ampm && (int) a == 0))
            a += 12;
        
        int old = this.hora;
        hora = (int) a;
        changeSupport.firePropertyChange("hora", old, hora);
    }
    
    public int getHora() {
        return hora;
    }
    
    public static double[] getXYByHora(int hora) {
        if (hora < 0 || hora > 23)
            throw new IllegalArgumentException("A hora deve estar entre 0 e 23");
        
        if (hora > 11)
            hora -= 12;
        
        Quadrante q = Quadrante.getByHora(hora);
        
        double x = 0;
        double y = 0;
        
        switch (q.getNum()) {
            case 1:
                x = Seno.getCatetoOposto(q.getHoras().get(hora), 75);
                y = Cosseno.getCatetoAdjascente(q.getHoras().get(hora), 75);
                break;
                
            case 2:
                x = Cosseno.getCatetoAdjascente(q.getHoras().get(hora), 75);
                y = new BigDecimal(Seno.getCatetoOposto(q.getHoras().get(hora), 75)).negate().doubleValue();
                break;
                
            case 3:
                x = new BigDecimal(Seno.getCatetoOposto(q.getHoras().get(hora), 75)).negate().doubleValue();
                y = new BigDecimal(Cosseno.getCatetoAdjascente(q.getHoras().get(hora), 75)).negate().doubleValue();
                break;
                
            case 4:
                x = new BigDecimal(Cosseno.getCatetoAdjascente(q.getHoras().get(hora), 75)).negate().doubleValue();
                y = Seno.getCatetoOposto(q.getHoras().get(hora), 75);
                break;
        }
        
        return new double[]{x, y};
    }

    @Override
    public String toString() {
        return Integer.toString(getHora());
    }
    
    public void addPropertyChangeListener(PropertyChangeListener l) {
        changeSupport.addPropertyChangeListener(l);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changeSupport.removePropertyChangeListener(l);
    }
    
    public static void main(String[] args) {
        for (int i = 0; i < 12; i++) {
            double[] xy = getXYByHora(i);
            System.out.println(i + " = " + xy[0] + "," + xy[1] + "\n");
        }
    }
}
