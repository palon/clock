/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.engine;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Paulo
 */
public enum Quadrante {
    Q1(1, 0),
    Q2(2, 90),
    Q3(3, 180),
    Q4(4, 270);
    
    private final int num;
    private final int ang;
    private final Map<Integer, Integer> horas;
    private final Map<Integer, Integer> minutos;

    private Quadrante(int num, int ang) {
        this.num = num;
        this.ang = ang;
        this.horas = new HashMap<>();
        this.minutos = new HashMap<>();
        
        switch (num) {
            case 1:
                horas.put(0, 0);
                horas.put(1, 30);
                horas.put(2, 60);
                
                minutos.put(0, 0);
                minutos.put(1, 6);
                minutos.put(2, 12);
                minutos.put(3, 18);
                minutos.put(4, 24);
                minutos.put(5, 30);
                minutos.put(6, 36);
                minutos.put(7, 42);
                minutos.put(8, 48);
                minutos.put(9, 54);
                minutos.put(10, 60);
                minutos.put(11, 66);
                minutos.put(12, 72);
                minutos.put(13, 78);
                minutos.put(14, 84);
                
                break;
                
            case 2:
                horas.put(3, 0);
                horas.put(4, 30);
                horas.put(5, 60);
                
                minutos.put(15, 0);
                minutos.put(16, 6);
                minutos.put(17, 12);
                minutos.put(18, 18);
                minutos.put(19, 24);
                minutos.put(20, 30);
                minutos.put(21, 36);
                minutos.put(22, 42);
                minutos.put(23, 48);
                minutos.put(24, 54);
                minutos.put(25, 60);
                minutos.put(26, 66);
                minutos.put(27, 72);
                minutos.put(28, 78);
                minutos.put(29, 84);
                
                break;
                
            case 3:
                horas.put(6, 0);
                horas.put(7, 30);
                horas.put(8, 60);
                
                minutos.put(30, 0);
                minutos.put(31, 6);
                minutos.put(32, 12);
                minutos.put(33, 18);
                minutos.put(34, 24);
                minutos.put(35, 30);
                minutos.put(36, 36);
                minutos.put(37, 42);
                minutos.put(38, 48);
                minutos.put(39, 54);
                minutos.put(40, 60);
                minutos.put(41, 66);
                minutos.put(42, 72);
                minutos.put(43, 78);
                minutos.put(44, 84);
                
                break;
                
            case 4:
                horas.put(9, 0);
                horas.put(10, 30);
                horas.put(11, 60);
                
                minutos.put(45, 0);
                minutos.put(46, 6);
                minutos.put(47, 12);
                minutos.put(48, 18);
                minutos.put(49, 24);
                minutos.put(50, 30);
                minutos.put(51, 36);
                minutos.put(52, 42);
                minutos.put(53, 48);
                minutos.put(54, 54);
                minutos.put(55, 60);
                minutos.put(56, 66);
                minutos.put(57, 72);
                minutos.put(58, 78);
                minutos.put(59, 84);
                
                break;
        }
    }

    public int getNum() {
        return num;
    }

    public int getAng() {
        return ang;
    }

    /**
     * Retorna um Map onde a chave é a hora e o valor é o ângulo da hora. Este
     * Map contém apenas as horas do quadrante em questão. É importante observar
     * que o ângulo da hora é relativo ao quadrante e não ao relógio, por
     * exemplo, hora 5 terá um ângulo de 60 graus relativo ao quadrante 2.
     * 
     * @return Map
     */
    public Map<Integer, Integer> getHoras() {
        return horas;
    }

    /** Retorna um Map onde a chave é o minuto e o valor é o ângulo da hora. Este
     * Map contém apenas os minutos do quadrante em questão. É importante observar
     * que o ângulo da hora é relativo ao quadrante e não ao relógio, por
     * exemplo, minuto 25 terá um ângulo de 60 graus relativo ao quadrante 2.
     * 
     * @return Map
     */
    public Map<Integer, Integer> getMinutos() {
        return minutos;
    }
    
    public static Quadrante getByXY(double x, double y) {
        if (x >= 0 && y > 0)
            return Q1;
        else if (x > 0 && y <= 0)
            return Q2;
        else if (x <= 0 && y < 0)
            return Q3;
        else
            return Q4;
    }
    
    public static Quadrante getByHora(int hora) {
        if (hora > 11)
            throw new IllegalArgumentException("A hora deve estar entre 0 e 11");
        
        for (Quadrante q : values())
            if (q.getHoras().keySet().contains(hora))
                return q;
        
        return null;
    }
    
    public static Quadrante getByMinuto(int minuto) {
        if (minuto > 59)
            throw new IllegalArgumentException("O minuto deve estar entre 0 e 59");
        
        for (Quadrante q : values())
            if (q.getMinutos().keySet().contains(minuto))
                return q;
        
        return null;
    }
}
