/*
 * Esta classe limita o número de caracteres inseridos em um JTextField
 * Para usar este Document: ((AbstractDocument)jTextField.getDocuement()).setDocumentFilter(new FixedLengthDocument(5));
 */
package br.view.ctrl;

import java.util.Arrays;
import java.util.List;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 *
 * @author Paulo Alonso
 */
public class TimeFieldDocument extends DocumentFilter {    
    private final List<Character> allow = Arrays.<Character>asList(new Character[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'});
   
    /**
     * 
     * @param fieldType TimeFieldDocument.HOUR ou TimeFieldDocument.MINUTE
     */
    public TimeFieldDocument() {
        super();
    }
    
    @Override
    public void insertString(FilterBypass fb, int offset, String str, AttributeSet attr) throws BadLocationException {
        // Método não utilizado, mas sua implementação é obrigatória
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String str, AttributeSet attr) throws BadLocationException {
        if (str == null)
            return;
        
        for (int i = 0; i < str.length(); i++) {
            if (!allow.contains(str.charAt(i)))
                return;
        }
        
        String old = "";
        
        try {
            old = fb.getDocument().getText(0, 2).trim();
        } catch (Exception ex) {
        }
        
        try {
            if(length == 0) {
                fb.insertString(offset, str, attr);
            } else {
                fb.replace(offset, length, str, attr);
            }
        } catch (Exception ex) {
            fb.replace(0, 2, old, attr);
        }
    }
}