/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.view;

import br.engine.Hora;
import br.engine.Minuto;
import br.engine.Quadrante;
import br.math.Pitagoras;
import br.math.Seno;
import br.view.ctrl.TimeFieldDocument;
import com.sun.glass.events.KeyEvent;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.time.LocalTime;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.text.AbstractDocument;
import org.jdesktop.beansbinding.AutoBinding.UpdateStrategy;
import org.jdesktop.beansbinding.BeanProperty;
import org.jdesktop.beansbinding.Binding;
import org.jdesktop.beansbinding.BindingGroup;
import org.jdesktop.beansbinding.Bindings;
import org.jdesktop.beansbinding.Converter;
import org.jdesktop.beansbinding.ELProperty;

/**
 *
 * @author Paulo
 */
public class Clock extends javax.swing.JPanel {
    private final PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private boolean setHora = true;
    
    /**
     * Distância entre o ponteiro do mouse e o centro do relógio no eixo X
     */
    private double x;
    /**
     * Distância entre o ponteiro do mouse e o centro do relógio no eixo Y
     */
    private double y;
    
    private Hora hora;
    private Minuto minuto;
    
    private LocalTime time;

    /**
     * Creates new form Clock
     */
    public Clock() {
        initComponents();
        
        this.hora = new Hora(0, 75);
        this.minuto = new Minuto(0, 75);
        
        ((AbstractDocument) txtHora.getDocument()).setDocumentFilter(new TimeFieldDocument());
        ((AbstractDocument) txtMinuto.getDocument()).setDocumentFilter(new TimeFieldDocument());
        
        BindingGroup bg = new BindingGroup();
        
        Converter c = new Converter() {
            @Override
            public Object convertForward(Object value) {
                if (value.toString().length() == 1)
                    return "0" + value;
                else
                    return value.toString();
            }

            @Override
            public Object convertReverse(Object value) {
//                if (Integer.parseInt(value.toString()) < 10)
//                    value = value.toString().replaceFirst("0", "");
                
                return Integer.parseInt(value.toString());
            }
        };
        
        Binding b1 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, hora, ELProperty.create("${ampm}"), btnMode, BeanProperty.create("selected"));
        Binding b2 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, hora, ELProperty.create("${pm}"), btnAmPm, BeanProperty.create("selected"));
        Binding b3 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, hora, ELProperty.create("${hora}"), txtHora, BeanProperty.create("text"));
        b3.setConverter(c);
        Binding b4 = Bindings.createAutoBinding(UpdateStrategy.READ_WRITE, minuto, ELProperty.create("${minuto}"), txtMinuto, BeanProperty.create("text"));
        b4.setConverter(c);
        
        bg.addBinding(b1);
        bg.addBinding(b2);
        bg.addBinding(b3);
        bg.addBinding(b4);
        
        bg.bind();
        
        hora.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            switch (evt.getPropertyName()) {
                case "ampm":
                    if (evt.getNewValue().equals(true)) {
                        btnMode.setText("AM/PM");
                    } else {
                        btnMode.setText("24h");
                    }
                    break;
                case "pm":
                    if (evt.getNewValue().equals(true))
                        btnAmPm.setText("PM");
                    else
                        btnAmPm.setText("AM");
                    break;
                case "hora":
                    iconePontHora();
//                    Trecho comentado para otimização da interface.
//                    Quando é feito um binding, dependendo do processo que é feito na
//                    na classe vinculada, a mudança de posição do ponteiro pode ficar lenta
//                    configTime();
            }
        });
        
        minuto.addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals("minuto")) {
                iconePontMinuto();
//                Trecho comentado para otimização da interface.
//                Quando é feito um binding, dependendo do processo que é feito na
//                na classe vinculada, a mudança de posição do ponteiro pode ficar lenta
//                configTime();
            }
        });
    }

    public Clock(int hora, int minuto) {
        super();
        
        setHora(hora);
        setMinuto(minuto);
    }

    public Clock(LocalTime time) {
        super();
        setTime(time);
    }

    public Hora getHora() {
        return hora;
    }
    
    public final void setHora(int hora) {
        if (hora > 11) {
            this.hora.setAmpm(false);
            this.hora.setPm(true);
        }
        
        double[] xy = Hora.getXYByHora(hora);
        
        x = xy[0];
        y = xy[1];
        
        setHoraByXY();
    }

    public Minuto getMinuto() {
        return minuto;
    }
    
    public final void setMinuto(int minuto) {
        double[] xy = Minuto.getXYByMinuto(minuto);
        
        x = xy[0];
        y = xy[1];
        
        setMinutoByXY();
    }
    
    /**
     * Retorna o horário marcado encapsulado em um java.time.LocalTime
     * É importante observar que sempre será no formato 24h, independente do modo
     * que está selecionado.
     * 
     * @return LocalTime
     */
    public LocalTime getTime() {
        return time;
    }
    
    public final void setTime(LocalTime time) {
        if (time == null) {
            setHora(0);
            setMinuto(0);
            
            return;
        }
        
        LocalTime old = this.time;
        this.time = time;
        changeSupport.firePropertyChange("time", old, time);
        
        setHora(time.getHour());
        setMinuto(time.getMinute());
    }
    
    private void configTime() {        
        LocalTime old = this.time;
        this.time = LocalTime.of(hora.getHora(), minuto.getMinuto());
        changeSupport.firePropertyChange("time", old, this.time);
    }
    
    private void setMinutoByXY() {
        minuto.setX(x);
        minuto.setY(y);
//        Trecho comentado para otimização da interface.
//        Quando é feito um binding, dependendo do processo que é feito na
//        na classe vinculada, a mudança de posição do ponteiro pode ficar lenta
//        configTime();
        iconePontMinuto();
    }
    
    private void setHoraByXY() {
        hora.setX(x);
        hora.setY(y);
//        Trecho comentado para otimização da interface.
//        Quando é feito um binding, dependendo do processo que é feito na
//        na classe vinculada, a mudança de posição do ponteiro pode ficar lenta
//        configTime();
        iconePontHora();
    }
    
    private void iconePontHora() {
        Integer hr = hora.getHora();
        
        if (hr > 11)
            hr -= 12;
        
        lblPontHora.setIcon(new ImageIcon(getClass().getResource("/br/view/img/ponteiro/hora/" + hr + ".png")));
    }
    
    private void iconePontMinuto() {
        Integer min = minuto.getMinuto();
        
        lblPontMinuto.setIcon(new ImageIcon(getClass().getResource("/br/view/img/ponteiro/minuto/" + min + ".png")));
    }
    
    private void onHoraFocus() {
        txtMinuto.setBackground(new Color(240, 240, 240));
        txtHora.setBackground(Color.WHITE);
        setHora = true;
    }
    
    private void onMinutoFocus() {
        txtHora.setBackground(new Color(240, 240, 240));
        txtMinuto.setBackground(Color.WHITE);
        setHora = false;
    }
    
    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        changeSupport.addPropertyChangeListener(l);
    }
    
    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        changeSupport.removePropertyChangeListener(l);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblClock = new javax.swing.JLabel();
        btnMode = new javax.swing.JToggleButton();
        btnAmPm = new javax.swing.JToggleButton();
        jLabel3 = new javax.swing.JLabel();
        lblPontHora = new javax.swing.JLabel();
        lblPontMinuto = new javax.swing.JLabel();
        lblClockBg = new javax.swing.JLabel();
        txtHora = new javax.swing.JTextField();
        txtMinuto = new javax.swing.JTextField();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblClock.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblClock.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/view/img/clock.png"))); // NOI18N
        lblClock.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                lblClockMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                lblClockMouseMoved(evt);
            }
        });
        lblClock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblClockMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblClockMouseReleased(evt);
            }
        });
        add(lblClock, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 160, -1));

        btnMode.setText("24h"); // NOI18N
        add(btnMode, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 90, -1));

        btnAmPm.setText("AM");
        add(btnAmPm, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 0, 90, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText(":");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, 20, 20));

        lblPontHora.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPontHora.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/view/img/ponteiro/hora/0.png"))); // NOI18N
        add(lblPontHora, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 160, -1));

        lblPontMinuto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPontMinuto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/view/img/ponteiro/minuto/0.png"))); // NOI18N
        add(lblPontMinuto, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 160, -1));

        lblClockBg.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblClockBg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/view/img/clock_background.png"))); // NOI18N
        add(lblClockBg, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 160, -1));

        txtHora.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtHora.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtHora.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtHoraFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtHoraFocusLost(evt);
            }
        });
        txtHora.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtHoraKeyPressed(evt);
            }
        });
        add(txtHora, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 190, 80, -1));

        txtMinuto.setBackground(new java.awt.Color(240, 240, 240));
        txtMinuto.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtMinuto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtMinuto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMinutoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMinutoFocusLost(evt);
            }
        });
        txtMinuto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMinutoKeyPressed(evt);
            }
        });
        add(txtMinuto, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 190, 80, -1));
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Obtém a distância (nos eixos x e y) entre o ponteiro do mouse e o centro
     * do relógio quando o mouse é movido. Este método leva em conta que o 
     * relógio (lblClock) sempre terá dimenções 160 x 150, uma vez que essa é a
     * dimenção da imagem PNG que representa o relógio mais as bordas verticais.
     * @param evt 
     */
    private void lblClockMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblClockMouseMoved
        x = evt.getX() - 80;
        y = Math.negateExact(evt.getY() - 75);
        
//        System.out.printf("%d, %d\n", x, y);
    }//GEN-LAST:event_lblClockMouseMoved
    
    private void lblClockMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblClockMouseDragged
        lblClockMouseMoved(evt);
        
        if (setHora)
            setHoraByXY();
        else
            setMinutoByXY();
    }//GEN-LAST:event_lblClockMouseDragged

    private void lblClockMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblClockMousePressed
        // Obtém o ângulo do click
        Quadrante q = Quadrante.getByXY(x, y);
        double t = new Pitagoras(x, y, Pitagoras.A).getA();
        int ang = 0;
        
        switch (q.getNum() % 2) {
            case 0:
                ang = (int) new Seno(y, t).getAlpha();
                break;
            case 1:
                ang = (int) new Seno(x, t).getAlpha();
                break;
        }
        
        // Ângulo de hora e minuto atuais
        int h = hora.getHora() > 11 ? hora.getHora() - 12 : hora.getHora();
        Quadrante qHora = Quadrante.getByHora(h);
        int angHora = qHora.getHoras().get(h);
        Quadrante qMinuto = Quadrante.getByMinuto(minuto.getMinuto());
        int angMinuto = qMinuto.getMinutos().get(minuto.getMinuto());
        
        /* Se o ângulo do clique mais ou menos a tolerância (tol) coincide com o ângulo de
         * hora ou minuto, informa quem será editado e sai do método. Isso faz
         * com que os ponteiros sejam selecionados ao clicar sobre eles.
        */        
        int tol = 6;
        int pontHora = 33;
        int pontMin = 45;
        
        if (q.equals(qHora) && t <= pontHora && ang - angHora >= Math.negateExact(tol) && ang - angHora <= tol) {
            setHora = true;
            onHoraFocus();
            return;
        } else if (q.equals(qMinuto) && t > pontHora && t < pontMin && ang - angMinuto >= Math.negateExact(tol) && ang - angMinuto <= tol) {
            setHora = false;
            onMinutoFocus();
            return;
        }

        // Só chega neste trecho se o clique não foi sobre o ponteiro
        if (setHora)
            setHoraByXY();
        else
            setMinutoByXY();
    }//GEN-LAST:event_lblClockMousePressed

    private void txtHoraFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHoraFocusGained
        onHoraFocus();
        txtHora.selectAll();
    }//GEN-LAST:event_txtHoraFocusGained

    private void txtMinutoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMinutoFocusGained
        onMinutoFocus();
        txtMinuto.selectAll();
    }//GEN-LAST:event_txtMinutoFocusGained

    private void lblClockMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblClockMouseReleased
        configTime();
    }//GEN-LAST:event_lblClockMouseReleased

    private void txtHoraFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtHoraFocusLost
        configTime();
    }//GEN-LAST:event_txtHoraFocusLost

    private void txtMinutoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMinutoFocusLost
        configTime();
    }//GEN-LAST:event_txtMinutoFocusLost

    private void txtHoraKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtHoraKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER)
            configTime();
    }//GEN-LAST:event_txtHoraKeyPressed

    private void txtMinutoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMinutoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER)
            configTime();
    }//GEN-LAST:event_txtMinutoKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton btnAmPm;
    private javax.swing.JToggleButton btnMode;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblClock;
    private javax.swing.JLabel lblClockBg;
    private javax.swing.JLabel lblPontHora;
    private javax.swing.JLabel lblPontMinuto;
    private javax.swing.JTextField txtHora;
    private javax.swing.JTextField txtMinuto;
    // End of variables declaration//GEN-END:variables

    public static void main(String[] args) {
        Clock c = new Clock();
        c.setHora(3);
        c.setMinuto(0);
        
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        f.getContentPane().setLayout(new BoxLayout(f.getContentPane(), BoxLayout.LINE_AXIS));
        f.getContentPane().add(c);
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
